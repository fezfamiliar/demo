var index int

func permute(nums []int) [][]int {

    index = 0
    k := 0
    n := len(nums)
    faker := make([]int, n)
    
   
    for i := 0; i < n; i++ {
 
        faker[i] = nums[i]
    }

    result:= make([][]int, fact(n))

    for i := range result {
        result[i] = make([]int, n)
}
    
 
    flag := make([]bool, n)
    
    exec(k, n, nums, flag, result, faker)
 
    return result
}



func exec(k int, n int, sol[] int, flag[] bool, result [][]int, faker[] int){
    
    
    if k >= n{

    for j := 0; j < n; j++ {

            result[index][j] = sol[j]
        }
        
        index++
       
    
    
    }else{
        
        for i := 0; i < n; i++ {
            if flag[i] == false{
                
                flag[i] = true
              
                    sol[k] = faker[i]
                
                
                exec(k + 1, n, sol, flag, result, faker)
                flag[i] = false
            }
        }
        
    }
    
}

func fact(n int) int{
    
    result := 1
    for i := 1; i <= n; i++ {
        
        result *= i
    }
    
    return result
}