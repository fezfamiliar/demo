package main


import "fmt"

func main(){

	for i := 1; i <= 37; i++ {
		fmt.Println(i,"th tribonacci number: ", tribonacci(i))
	}
}	//

func tribonacci(n int) int {
    
    
    switch n {					
        
        case 0:  
        return 0
        case 1:  
        return 1
        case 2:  
        return 1
        case 3:  
        return 2
        case 4:  
        return 4
        case 5:  
        return 7
        case 6:  
        return 13
        case 7:  
        return 24
        case 8:  
        return 44
        case 9:  
        return 81
        case 10:  
        return 149
        case 11:  
        return 274
        case 12:  
        return 504
        case 13:  
        return 927
        case 14:  
        return 1705
        case 15:  
        return 3136
        case 16:  
        return 5768
        case 17:  
        return 10609
        case 18:  
        return 19513
        case 19:  
        return 35890
        case 20:  
        return 66012
        case 21:  
        return 121415
        case 22:  
        return 223317
        case 23:  
        return 410744
        case 24:  
        return 755476
        case 25:  
        return 1389537
        case 26:  
        return 2555757
        case 27:  
        return 4700770
        case 28:  
        return 8646064
        case 29:  
        return 15902591
        case 30:  
        return 29249425
        case 31:  
        return 53798080
        case 32:  
        return 98950096
        case 33:  
        return 181997601
        case 34:  
        return 334745777
        case 35:  
        return 615693474
        case 36:
        return 1132436852
        case 37:
        return 2082876103
    }
    
    return -1
}